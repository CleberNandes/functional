import { handleError, log } from './utils/handleError.js';
import './utils/arrayHelpers.js';
import { notasService as service } from './nota/nota.service.js'


document.querySelector('#myButton').onclick = () => 
    service
    .sumItems('2143')
    .then(log)
    .catch(console.log);
