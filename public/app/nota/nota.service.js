import { handleError } from '../utils/handleError.js';

const API = 'http://localhost:3000/notas';

const sumItems = code => notas => notas
    .flatMap(nota => nota.itens)
    .filter(item => item.codigo == code)
    .sum(item => item.valor);

export const notasService = {

    index() {
        return fetch(API).then(handleError);
    },

    sumItems(code) {
        return this.index().then(sumItems(code));
    }
} 