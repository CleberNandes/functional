export const handleError = (res) => 
    res.ok ? res.json() : Promise.reject(res.statusText);

export const log = (res) => {
    console.log(res);
    return res;
}

export const flatMap = (res) => 
    res.reduce((total, daVez) => 
        total.concat(daVez));

    