if(!Array.prototype.flatMap) {
    Array.prototype.flatMap = function(callback) {
        return this.map(callback).reduce((total, itens) => total.concat(itens), []);
    }
}

if(!Array.prototype.sum) {
    Array.prototype.sum = function(callback) {
        return this.map(callback).reduce((total, item) => total + item, 0);
    }
}